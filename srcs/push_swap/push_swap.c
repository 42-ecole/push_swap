/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 12:31:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/25 13:57:24 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static t_stack		*gl_chunks;
static t_advstack	*end_stack;
static int			g_chunk_depth;
static	bool		first_remain;

void		print_fel(void *obj, void *data)
{
	int		num;

	num = *CAST_TYPE(int, obj);
	printf(" %d\n", num);
}


int		get_mid(t_list *lst)
{
	int		*mid;
	t_list	*sorted;

	sorted = new_list(lst->capacity, lst->content_size);
	sorted->count = lst->count;
	add_range(sorted->enumerable, sorted,
			lst->enumerable, lst, 0, sorted->count);
	// printf("count: %d\n", sorted->count - 1);
	lst_sort(sorted, 0, sorted->count - 1);
	// make this for 
	mid = (int *)sorted->index_of(sorted, sorted->count / 2);
	free(sorted);
	return (*mid);
}

void		part_one(t_stack *a, t_stack *b, t_stack *chunks, bool origin)
{
	int		mid;
	int		chunk_size;
	int		i;
	int		num;
	int		to_reverse;

	if (a->seq->count == 2)
	{
		if (*CAST_TYPE(int, a->seq->index_of(a->seq, 0))
		< *CAST_TYPE(int, a->seq->index_of(a->seq, 1)))
		{
			sa(a, b);
			printf("sa\n");
		}
		return;
	}
	else
	{
		to_reverse = 0;
		i = chunk_size = (a->seq->count / 2);
		chunks->push(chunks, &chunk_size);
		mid = get_mid(a->seq);
		while (i)
		{
			num = *CAST_TYPE(int, a->peek(a));
			
			if (num < mid)
			{
				pb(a, b);
				printf("pb\n");
				i--;
			}
			else
			{
				ra(a, b);
				printf("ra\n");
			}
		}
	}
	if (a->seq->count > 1)
		part_one(a, b, chunks, true);
}

void		chunk_remains(t_advstack *src, t_advstack *dst, int size, int depth)
{
	int		obj;
	int		obj2;
	
	if (size == 2)
		src->swap(src, dst);
	while (size)
	{
		src->push(src, dst);
		size--;
	}
}

void		print_chunk(t_stack *s, int size)
{
	t_list	*lst;

	lst = new_sublist(s->seq, s->seq->count - size, size);
	printf("---Chunk---\n");
	foreach(s->seq->enumerable, s->seq, print_fel, NULL);
	printf("-----------\n");
	free(lst);
}

int			chunk_depth(int size)
{
	// int		i;

	// i = 0;
	// while (size > 1)
	// {
	// 	size /= 2;
	// 	i++;
	// }
	return (2 * (size / 6) + 1);
}

// Returns true, if count of recurive calls is odd.
bool		is_odd_callback()
{
	if (g_chunk_depth % 2 == 0)
		return (true);
	return (false);
}

bool		is_chunk_sorted(t_advstack *s, int size)
{
	t_list	*chunk;

	chunk = new_sublist(s->s->seq, s->s->seq->count - size, size);
	if (s->s->seq->is_sorted(chunk))
	{
		free(chunk);
		return (true);
	}
	free(chunk);	
	return (false);
}

void		make_chunk(t_advstack *src, t_advstack *dst, int size, int depth)
{
	int		sent_size;
	int		remain_size;
	
	if (size < 3)
		chunk_remains(src, dst, size, depth);
	else
	{
		remain_size = src->push_with_if(src, dst, size, gl_chunks->seq->count);
		sent_size = size - remain_size;

		// Finish order of pushed chunk recursively.
		if (remain_size)
		{
			if (remain_size < 3)
			{
				// check if pushed to B finally
				if (depth % 2 != 0)
					make_chunk(src, dst, remain_size, depth);
			}
			else
			{
				if (!is_chunk_sorted(src, remain_size))
					make_chunk(src, dst, remain_size, depth - 1);
			}
		}
		if (sent_size)
		{
			if (sent_size < 3)
			{
				// check if pushed to B finally
				if (depth % 2 == 0)
				{
					chunk_remains(src, dst, sent_size, depth);
				}
					// make_chunk(dst, src, sent_size, depth);
			}
			else
			{
				if (!is_chunk_sorted(dst, sent_size))
					make_chunk(src, dst, sent_size, depth - 1);
			}
		}
	}
}

bool		gen_sub_chunk(t_advstack *src, t_advstack *dst, int size, int depth)
{
	t_list	*chunk;
	int		callback_chunk;
	
	chunk = new_sublist(dst->s->seq, dst->s->seq->count - size, size);
	// callback recurse sorting.
	if (!dst->s->seq->is_sorted(chunk))
	{
		// We add new chunk to callback chunk
		if (size > 2)
		{
			// gl_chunks->push(gl_chunks, &callback_chunk);
			make_chunk(dst, src, size, depth - 1);
		}
		else if (size == 2)
		{
			dst->swap(dst, src);
		}
		return (true);
	}
	free(chunk);
	return (false);
}

int		main(int argc, char *argv[])
{
	t_stacks	*stacks;

	if (argc < 2)
		return (-1);
	init_logger();
	stacks = input_to_stack(argc, argv);

	gl_chunks = new_stack(stacks->a->s->seq->capacity, stacks->a->s->seq->content_size);
	part_one(stacks->a->s, stacks->b->s, gl_chunks, true);
	
	end_stack = stacks->a;
	first_remain = false;
	printf("---Chunks---\n");
	foreach(gl_chunks->seq->enumerable, gl_chunks->seq, print_fel, NULL);
	printf("---Pre A--\n");
	foreach(stacks->a->s->seq->enumerable, stacks->a->s->seq, print_fel, NULL);
	printf("---Pre B--\n");
	foreach(stacks->b->s->seq->enumerable, stacks->b->s->seq, print_fel, NULL);
	while (gl_chunks->seq->count)
	{
		int chunk =  *CAST_TYPE(int,gl_chunks->pop(gl_chunks));
		int depth = chunk_depth(chunk);
		make_chunk(stacks->b, stacks->a, chunk, depth);
		printf("---Stack A--\n");
		foreach(stacks->a->s->seq->enumerable, stacks->a->s->seq, print_fel, NULL);
		printf("---Stack B--\n");
		foreach(stacks->b->s->seq->enumerable, stacks->b->s->seq, print_fel, NULL);
		printf("___________\n");
	}

	printf("---Stack A--\n");
	foreach(stacks->a->s->seq->enumerable, stacks->a->s->seq, print_fel, NULL);
	printf("---Stack B--\n");
	foreach(stacks->b->s->seq->enumerable, stacks->b->s->seq, print_fel, NULL);
	return (0);
}

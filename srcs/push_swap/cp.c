/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cp.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 12:31:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/13 18:29:36 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static t_stack		*chunks;

void		print_fel(void *obj, void *data)
{
	int		num;

	num = *CAST_TYPE(int, obj);
	printf(" %d\n", num);
}


int		get_mid(t_list *lst)
{
	int		*mid;
	t_list	*sorted;

	sorted = new_list(lst->capacity, lst->content_size);
	sorted->count = lst->count;
	add_range(sorted->enumerable, sorted,
			lst->enumerable, lst, 0, sorted->count);
	// printf("count: %d\n", sorted->count - 1);
	lst_sort(sorted, 0, sorted->count - 1);
	// make this for 
	mid = (int *)sorted->index_of(sorted, sorted->count / 2);
	free(sorted);
	return (*mid);
}

int		get_mid2(t_list *lst, int start, int len)
{
	int		*mid;
	t_list	*sorted;

	sorted = new_list(len, lst->content_size);
	// rework, adding method to delete this
	sorted->count = len;
	add_range(sorted->enumerable, sorted,
			lst->enumerable, lst, start, len);
	// printf("---mid B--\n");
	// foreach(sorted->enumerable, sorted, print_fel, NULL);
	// printf("count: %d\n", sorted->count - 1);
	lst_sort(sorted, 0, sorted->count - 1);
	// make this for 
	mid = (int *)sorted->index_of(sorted, sorted->count / 2);
	free(sorted);
	return (*mid);
}

int		top_to_bot(t_stack *a, t_stack *b, int mid)
{
	int		num;
	int		i;

	i = 0;
	while (true)
	{
		num = *CAST_TYPE(int, a->peek(a));
		if (num < mid)
		{
			pb(a, b);
			printf("pb\n");
			i++;
		}
		else
			break;
	}
	return (i);
}

int		bot_to_top(t_stack *a, t_stack *b, int mid)
{
	int		num;
	int		i;

	i = 0;
	while (true)
	{
		num = *CAST_TYPE(int, a->seq->index_of(a->seq, i));
		if (num < mid)
		{
			pb(a, b);
			printf("pb\n");
			i++;
		}
		else
			break;
	}
	return (i);
}

void		part_one(t_stack *a, t_stack *b, t_stack *chunks)
{
	int		mid;
	int		chunk_size;
	int		i;
	int		num;
	int		to_reverse;

	to_reverse = 0;
	mid = get_mid(a->seq);
	chunk_size = (a->seq->count / 2);
	chunks->push(chunks, &chunk_size);
	i = chunk_size;
	while (i)
	{
		int num = *CAST_TYPE(int, a->peek(a));
		
		if (num < mid)
		{
			pb(a, b);
			printf("pb\n");
			i--;
		}
		else
		{
			ra(a, b);
			printf("ra\n");
		}
	}

	if (a->seq->count > 2)
		part_one(a, b, chunks);
	else if (a->seq->count == 2)
	{
		if (*CAST_TYPE(int, a->seq->index_of(a->seq, 0))
		< *CAST_TYPE(int, a->seq->index_of(a->seq, 1)))
		{
			sa(a, b);
			printf("sa\n");
		}
	}
}

void		little_chunk(t_stack *a, t_stack *b, int chunk_size)
{
	if (chunk_size == 1)
	{
		pa(a,b);
		printf("pa\n");
	}
	else if (chunk_size == 2)
	{
		int		obj;
		int		obj2;

		// do tests
		obj = *CAST_TYPE(int, b->seq->index_of(b->seq, b->seq->count - 1));
		obj2 = *CAST_TYPE(int, b->seq->index_of(b->seq, b->seq->count - 2));
		if (obj2 > obj)
		{
			sb(a, b);
			printf("sb\n");
		}
		pa(a, b);
		pa(a, b);
		printf("pa\npa\n");
	}
}

void		part_two(t_stack *a, t_stack *b, int chunk_size)
{
	int		mid;
	int		to_reverse;
	int		to_traverse;
	int		new_chunk = 0;
	int		i;

	// chunk_size = *CAST_TYPE(int, chunks->pop(chunks));
	if (chunk_size <= 2)
		little_chunk(a, b, chunk_size);
	else
	{
		to_reverse = 0;
		i = chunk_size / 2;
		if (!(chunk_size % 2))
			i--;
		to_traverse = chunk_size - i;
		// dont start if already sorted
		mid = get_mid2(b->seq, b->seq->count - chunk_size, chunk_size);//test
		// int buf = *CAST_TYPE(int, b->seq->index_of(b->seq, b->seq->count - chunk_size - 1));
		printf("mid: %d; traverse: %d\n", mid, to_traverse);
		// to_traverse = traverse_chunk(b, mid, chunk_size);
		int num;
		while (i)
		{
			num = *CAST_TYPE(int, b->peek(b));
			if (num > mid)
			{
				printf("---before--\n");
				foreach(b->seq->enumerable, b->seq, print_fel, NULL);
				pa(a, b);
				printf("pa\n");
				while (to_reverse && chunks->seq->count)
				{
					rrb(a, b);
					printf("rrb\n");
					to_reverse--;
				}
				new_chunk++;
				i--;
				// printf("---after--\n");
				// foreach(b->seq->enumerable, b->seq, print_fel, NULL);
			}
			else
			{
				rb(a, b);
				printf("rb\n");
				to_reverse++;
			}
		}
		if (new_chunk == 2)
		{
			if (*CAST_TYPE(int, a->seq->index_of(a->seq, 0))
			> *CAST_TYPE(int, a->seq->index_of(a->seq, 1)))
			{
				sa(a, b);
				printf("sa\n");
			}
		}
		else if (new_chunk > 2)
		{
			mid = get_mid2(a->seq, a->seq->count - new_chunk, new_chunk);//test
			to_reverse = 0;
			i = new_chunk / 2;
			if (!(new_chunk % 2))
				i--;
			while (i)
			{
				num = *CAST_TYPE(int, a->peek(a));
				if (num < mid)
				{
					pb(a, b);
					printf("pb\n");
					while (to_reverse && chunks->seq->count)
					{
						rra(a, b);
						printf("rra\n");
						to_reverse--;
					}
					new_chunk++;
					i--;
					// printf("---after--\n");
					// foreach(b->seq->enumerable, b->seq, print_fel, NULL);
				}
				else
				{
					ra(a, b);
					printf("ra\n");
					to_reverse++;
				}
			}
		}
		if (b->seq->count && to_traverse)
			part_two(a, b, to_traverse);
	}
	if (b->seq->count)
		part_two(a, b, *CAST_TYPE(int, chunks->pop(chunks)));
}

int		main(int argc, char *argv[])
{
	t_stacks	*stacks;

	if (argc < 2)
		return (-1);
	init_logger();
	stacks = input_to_stack(argc, argv);
	chunks = new_stack(stacks->a->seq->capacity, stacks->a->seq->content_size);

	// need to rework methods
	// its may be initialization of chunks, firstly. (Now it is in the part_one).
	// then need to rework part_two to use alghoritm for each stack recursively.
	part_one(stacks->a, stacks->b, chunks);
	part_two(stacks->a, stacks->b, *CAST_TYPE(int, chunks->pop(chunks)));

	printf("---Stack A--\n");
	foreach(stacks->a->seq->enumerable, stacks->a->seq, print_fel, NULL);
	printf("---Stack B--\n");
	foreach(stacks->b->seq->enumerable, stacks->b->seq, print_fel, NULL);
	printf("---Chunks---\n");
	foreach(chunks->seq->enumerable, chunks->seq, print_fel, NULL);




	return (0);
}

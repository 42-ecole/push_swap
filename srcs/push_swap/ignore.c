/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/30 12:31:40 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/13 16:52:50 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int		top_to_bot(t_stack *a, t_stack *b, int mid)
{
	int		num;
	int		i;

	i = 0;
	while (true)
	{
		num = *CAST_TYPE(int, a->peek(a));
		if (num < mid)
		{
			pb(a, b);
			printf("pb\n");
			i++;
		}
		else
			break;
	}
	return (i);
}

int		bot_to_top(t_stack *a, t_stack *b, int mid)
{
	int		num;
	int		i;

	i = 0;
	while (true)
	{
		num = *CAST_TYPE(int, a->seq->index_of(a->seq, i));
		if (num < mid)
		{
			pb(a, b);
			printf("pb\n");
			i++;
		}
		else
			break;
	}
	return (i);
}

static t_stack		*chunks;

void		print_fel(void *obj, void *data)
{
	int		num;

	num = *CAST_TYPE(int, obj);
	printf(" %d\n", num);
}


int		get_mid(t_list *lst)
{
	int		*mid;
	t_list	*sorted;

	sorted = new_list(lst->capacity, lst->content_size);
	sorted->count = lst->count;
	add_range(sorted->enumerable, sorted,
			lst->enumerable, lst, 0, sorted->count);
	// printf("count: %d\n", sorted->count - 1);
	lst_sort(sorted, 0, sorted->count - 1);
	// make this for 
	mid = (int *)sorted->index_of(sorted, sorted->count / 2);
	free(sorted);
	return (*mid);
}

int		get_middle(t_list *lst, int start, int len)
{
	int		*mid;
	t_list	*sorted;

	sorted = new_list(len, lst->content_size);
	sorted->count = len;
	add_range(sorted->enumerable, sorted,
			lst->enumerable, lst, start, len);
	lst_sort(sorted, 0, sorted->count - 1);
	mid = (int *)sorted->index_of(sorted, sorted->count / 2);
	free(sorted);
	return (*mid);
}

void		little_chunkA(t_stack *a, t_stack *b, int chunk_size)
{
	if (chunk_size == 2)
	{
		int		obj;
		int		obj2;

		// do tests
		obj = *CAST_TYPE(int, a->seq->index_of(a->seq, a->seq->count - 1));
		obj2 = *CAST_TYPE(int, a->seq->index_of(a->seq, a->seq->count - 2));
		if (obj2 > obj)
		{
			sa(a, b);
			printf("sa\n");
		}
	}
}

void		part_one(t_stack *a, t_stack *b, t_stack *chunks, bool origin)
{
	int		mid;
	int		chunk_size;
	int		i;
	int		num;
	int		to_reverse;

	to_reverse = 0;
	chunk_size = (a->seq->count / 2);
	chunks->push(chunks, &chunk_size);

	if (a->seq->count == 2)
	{
		if (*CAST_TYPE(int, a->seq->index_of(a->seq, 0))
		< *CAST_TYPE(int, a->seq->index_of(a->seq, 1)))
		{
			sa(a, b);
			printf("sa\n");
		}
	}
	else
	{
		i = chunk_size;
		mid = get_mid(a->seq);
		while (i)
		{
			int num = *CAST_TYPE(int, a->peek(a));
			
			if (num < mid)
			{
				pb(a, b);
				printf("pb\n");
				i--;
			}
			else
			{
				ra(a, b);
				printf("ra\n");
			}
		}
	}
	if (a->seq->count > 2)
		part_one(a, b, chunks, true);
}

void		little_chunk(t_stack *a, t_stack *b, int chunk_size)
{
	if (chunk_size == 1)
	{
		pa(a,b);
		printf("pa\n");
	}
	else if (chunk_size == 2)
	{
		int		obj;
		int		obj2;

		// do tests
		obj = *CAST_TYPE(int, b->seq->index_of(b->seq, b->seq->count - 1));
		obj2 = *CAST_TYPE(int, b->seq->index_of(b->seq, b->seq->count - 2));
		if (obj2 > obj)
		{
			sb(a, b);
			printf("sb\n");
		}
		pa(a, b);
		pa(a, b);
		printf("pa\npa\n");
	}
}

void		part_twoA(t_stack *a, t_stack *b, int chunk_size)
{
	int		mid;
	int		to_reverse;
	int		to_traverse;
	int		new_chunk = 0;
	int		i;

	// chunk_size = *CAST_TYPE(int, chunks->pop(chunks));
	if (chunk_size <= 2)
		little_chunk(b, a, chunk_size);
	else
	{
		to_reverse = 0;
		i = chunk_size / 2;
		if (!(chunk_size % 2))
			i--;
		to_traverse = i;
		// dont start if already sorted
		mid = get_middle(a->seq, a->seq->count - chunk_size, chunk_size);
		// int buf = *CAST_TYPE(int, b->seq->index_of(b->seq, b->seq->count - chunk_size - 1));
		// to_traverse = traverse_chunk(b, mid, chunk_size);
		int num;
		while (i)
		{
			num = *CAST_TYPE(int, a->peek(a));
			if (num > mid)
			{
				// foreach(b->seq->enumerable, b->seq, print_fel, NULL);
				pb(a, b);
				printf("pb\n");
				while (to_reverse && chunks->seq->count)
				{
					rra(a, b);
					printf("rra\n");
					to_reverse--;
				}
				new_chunk++;
				i--;
				// printf("---after--\n");
				// foreach(b->seq->enumerable, b->seq, print_fel, NULL);
			}
			else
			{
				ra(a, b);
				printf("ra\n");
				to_reverse++;
			}
		}
		// if (new_chunk >= 2)
		// {
		// 	// chunks->push(chunks, &new_chunk);
		// 	// part_one(a, b, chunks, false);
		// 	part_twoA(b, a, new_chunk);
		// }
		if (a->seq->count && chunk_size - to_traverse)
			part_twoA(a, b, chunk_size - to_traverse);
	}
	// if (b->seq->count)
	// {
	// 	// if (chunk_size - i)
	// 	// 	part_two(a, b, chunk_size - i);
	// 	// else
	// 		part_twoA(a, b, *CAST_TYPE(int, chunks->pop(chunks)));
	// }
}

void		chunk_remains(t_advstack *src, t_advstack *dst, int size)
{
	int		obj;
	int		obj2;

	if (size == 2)
	{
		obj = *CAST_TYPE(int,
			src->s->seq->index_of(src->s->seq, src->s->seq->count - 1));
		obj2 = *CAST_TYPE(int,
			src->s->seq->index_of(src->s->seq, src->s->seq->count - 2));
		if (obj2 > obj) // put into swap
			src->swap(src, dst);
	}
	while (size)
	{
		src->push(src, dst);
		size--;
	}
}

void		make_chunk(t_advstack *src, t_advstack *dst, int size)
{
	int		num;
	int		new_size;
	int		remain_size;
	int		mid;
	int		to_reverse;

	if (size < 3)
		chunk_remains(src, dst, size);
	else
	{
		to_reverse = 0;
		new_size = size / 2;
		if (!(size % 2))
			new_size--;
		remain_size = size - new_size;
		mid = get_middle(src->s->seq, src->s->seq->count - size, size);
		while (new_size)
		{
			num = *CAST_TYPE(int, src->s->peek(src->s));
			if (num > mid)
			{
				src->push(src, dst);
				while (to_reverse && chunks->seq->count)
				{
					src->rev_rot(src, dst);
					to_reverse--;
				}
				new_size--;
			}
			else
			{
				src->rot(src, dst);
				to_reverse++;
			}
		}
		if (src->s->seq->count && remain_size)
			make_chunk(src, dst, remain_size);
	}
	if (src->s->seq->count)
		make_chunk(src, dst, *CAST_TYPE(int, chunks->pop(chunks)));
}

void		part_two(t_stack *a, t_stack *b, int chunk_size)
{
	int		mid;
	int		to_reverse;
	int		to_traverse;
	int		new_chunk = 0;
	int		i;

	// chunk_size = *CAST_TYPE(int, chunks->pop(chunks));
	if (chunk_size <= 2)
		little_chunk(a, b, chunk_size);
	else
	{
		to_reverse = 0;
		i = chunk_size / 2;
		if (!(chunk_size % 2))
			i--;
		// dont start if already sorted
		mid = get_middle(b->seq, b->seq->count - chunk_size, chunk_size);
		int num;
		while (i)
		{
			num = *CAST_TYPE(int, b->peek(b));
			if (num > mid)
			{
				printf("---before--\n");
				foreach(b->seq->enumerable, b->seq, print_fel, NULL);
				pa(a, b);
				printf("pa\n");
				while (to_reverse && chunks->seq->count)
				{
					rrb(a, b);
					printf("rrb\n");
					to_reverse--;
				}
				new_chunk++;
				i--;
				// printf("---after--\n");
				// foreach(b->seq->enumerable, b->seq, print_fel, NULL);
			}
			else
			{
				rb(a, b);
				printf("rb\n");
				to_reverse++;
			}
		}
		if (new_chunk >= 2)
		{
			
		}
		if (b->seq->count && chunk_size)
			part_two(a, b, chunk_size);
	}
	if (b->seq->count)
		part_two(a, b, *CAST_TYPE(int, chunks->pop(chunks)));
}

int		main(int argc, char *argv[])
{
	t_stacks	*stacks;

	if (argc < 2)
		return (-1);
	init_logger();
	stacks = input_to_stack(argc, argv);
	chunks = new_stack(stacks->a->s->seq->capacity, stacks->a->s->seq->content_size);
	part_one(stacks->a->s, stacks->b->s, chunks, true);
	
	make_chunk(stacks->b, stacks->a, *CAST_TYPE(int, chunks->pop(chunks)));

	printf("---Stack A--\n");
	foreach(stacks->a->s->seq->enumerable, stacks->a->s->seq, print_fel, NULL);
	printf("---Stack B--\n");
	foreach(stacks->b->s->seq->enumerable, stacks->b->s->seq, print_fel, NULL);
	printf("---Chunks---\n");
	foreach(chunks->seq->enumerable, chunks->seq, print_fel, NULL);




	return (0);
}

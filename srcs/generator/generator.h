/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generator.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 08:31:22 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/10 11:59:30 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GENERATOR_H
# define GENERATOR_H

# include <stdlib.h>
# include <stdio.h>
# include "libft.h"

typedef enum
{
	ERROR,
	HELP,
	RANGE,
	COUNT
}		args_keys_state;

typedef struct	s_args_keys
{
	args_keys_state		state;
	void				(*exec)(void *data);
	void				*data;
}				t_args_keys;

typedef struct	s_range
{
	int			min;
	int			max;
}				t_range;

typedef struct	s_generator
{
	t_list		*lst;
	t_range		range;
	int			count;
}				t_generator;

typedef struct s_seed
{
  unsigned x; // начальные значения могут быть любыми
  unsigned y;
  unsigned z;
  unsigned w;
}				t_seed;

#endif
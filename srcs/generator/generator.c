/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   generator.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/10 08:31:42 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/20 12:29:18 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "generator.h"

static t_generator generator;

void		print_fel(void *obj, void *data)
{
	int		num;

	num = *CAST_TYPE(int, obj);
	printf("%d ", num);
}

unsigned XorShift128(int start)
{
  static t_seed s;

  if (s.x == 0 && s.y == 0 && s.z == 0)
  {
	  s.x = start % 23525;
	  s.y = start % 74;
	  s.z = start % 22;
	  s.w = start % 17;
  }
  unsigned t = s.x^(s.x<<11);
  s.x = s.y;
  s.y = s.z;
  s.z = s.w;
  s.w = (s.w^(s.w>>19)) ^ (t^(t>>8));
  return s.w;
}

void FizzleFade(unsigned width, unsigned height)
{
  unsigned x, y;
  long rndval = 1;
  do {
    y = (rndval & 0x000FF) - 1;  // младшие 8 бит - 1 = координата y
    x = (rndval & 0x1FF00) >> 8; // следующие 9 bits = координата x
    unsigned lsb = rndval & 1;   // младший бит потеряется при сдвиге
    rndval >>= 1;
    if (lsb) // если выдвинутый бит = 0, то не надо xor
      rndval ^= 0x00012000;

    
  } while (rndval != 1);
}

void			generate_rand(void *rng, void *out)
{
	t_range		*range;
	int			*num;
	
	range = (t_range *)rng;
	srand(time(NULL));
	num = out;
	*num = XorShift128(rand()) % range->max + range->min;
}

t_list		*gen_construct(void (*gen_func)(void *data, void *out),
							void *gen_data)
{
	t_list	*lst;
	t_range	*range;
	long	rndval;
	int		i;

	i = 0;
	range = (t_range *)gen_data;
	srand(time(NULL));
	rndval = rand();
	lst = new_list(generator.count, sizeof(int)); // rework sizeof.
	do {
		if (i >= generator.count)
			break;
		unsigned lsb = rndval & 1;   // младший бит потеряется при сдвиге
		rndval >>= 1;
		if (lsb) // если выдвинутый бит = 0, то не надо xor
		rndval ^= 0x00012000;
		rndval = rndval % range->max + range->min;
		lst->add(lst, &rndval);
		i++;
	} while (rndval != 1);
	return (lst);
}

t_args_keys	new_args_key(args_keys_state state, void *data,
							void (*func)(void *data))
{
	t_args_keys		package;

	package.state = state;
	package.exec = func;
	package.data = data;
	return (package);
}

t_range		new_range(int min, int max)
{
	t_range		range;

	range.min = min;
	range.max = max;
	return (range);
}

void		help_exec(void *data)
{
	printf("%s\n", (char *)data);
	exit(1);
}

void		range_exec(void *data)
{
	// generator.range = *CAST_TYPE(t_range, data);
	generator.range.min = *CAST_TYPE(int, data);
	generator.range.max = *CAST_TYPE(int, data);
	printf("rng: %d\n", generator.range.min);
	printf("rng: %d\n", generator.range.max);

}

void		count_exec(void *data)
{
	generator.count = *CAST_TYPE(int, data);
}

t_list		*handle_keys(int argc, char *argv[])
{
	int			i;
	t_list		*keys;
	t_args_keys	tmp;
	t_range		rng;
	int			count;

	i = 0;
	keys = new_list(argc, sizeof(t_args_keys));
	while (i < argc - 1)
	{
		//TODO: exclude options in outer functions
		if (!ft_strcmp(argv[i], "-h"))
		{
			tmp = new_args_key(HELP, "Little help\n", help_exec);
			keys->add(keys, &tmp);
		}
		else if (!ft_strcmp(argv[i], "-r"))
		{
			if (i + 2 > argc)
				exit(1);
			rng = new_range(ft_atoi(argv[i + 1]), ft_atoi(argv[i + 2]));
			count = 5;
			tmp = new_args_key(RANGE, &count, range_exec);
			keys->add(keys, &tmp);
		}
		else if (!ft_strcmp(argv[i], "-c"))
		{
			if (i + 1 > argc)
				exit(1);
			count = ft_atoi(argv[i + 1]);
			tmp = new_args_key(COUNT, &count, count_exec);
			keys->add(keys, &tmp);
		}
		//
		i++;
	}
	return (keys);
}

void		execute_keys(void *key, void *data)
{
	t_args_keys	*lkey;

	lkey = (t_args_keys *)key;
	lkey->exec(lkey->data);
}

int			main(int argc, char *argv[])
{
	t_list		*randl;
	t_list		*keys;

	generator.count = 50;
	generator.range.min = 0;
	generator.range.max = 100;
	if (argc > 1)
	{
		keys = handle_keys(argc, argv);
		foreach(keys->enumerable, keys, execute_keys, NULL);
		
		randl = gen_construct(generate_rand, &(generator.range));
		foreach(randl->enumerable, randl, print_fel, NULL);
	}
	return (0);
}
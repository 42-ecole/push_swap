/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sa.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 16:18:42 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/26 20:53:49 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operator.h"
#include "stdio.h"

void	sa(t_stack *a, t_stack *b)
{
	int		*el;
	int		*el2;
	int		num;
	int		num2;

	if ((el = CAST_TYPE(int, a->pop(a)))
	&& (el2 = CAST_TYPE(int, a->pop(a))))
	{
		num = *el;
		num2 = *el2;
		a->push(a, &num);
		a->push(a, &num2);
	}
}

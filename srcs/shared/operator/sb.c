/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sb.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 16:18:42 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/26 20:55:14 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "operator.h"

void	sb(t_stack *a, t_stack *b)
{
	int		*el;
	int		*el2;
	int		num;
	int		num2;

	if ((el = CAST_TYPE(int, b->pop(b)))
	&& (el2 = CAST_TYPE(int, b->pop(b))))
	{
		num = *el;
		num2 = *el2;
		b->push(b, &num);
		b->push(b, &num2);
	}
}

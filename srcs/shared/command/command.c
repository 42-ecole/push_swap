/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 15:45:27 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/09 18:54:45 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"

t_command	*new_command(char *type, void (*run)(t_stack *a, t_stack *b))
{
	t_command	*command;

	command = ft_memalloc(sizeof(t_command));
	command->type = type;
	command->run = run;
	return (command);
}

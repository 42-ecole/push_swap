/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_controller.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 17:51:57 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/13 18:30:59 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"

bool		select_by_type(void *src, void *type)
{
	t_command		*cmd;
	char			*c_type;
	t_object		*obj;

	obj = (t_object *)src;
	c_type = (char *)type;
	cmd = (t_command *)obj->content;
	if (!ft_strcmp(cmd->type, type))
		return (true);
	return (false);
}

void		print_cmd(void *wrap, void *data)
{
	t_command 		*cmd;
	t_object		*obj;

	obj = (t_object *)wrap;
	cmd = (t_command *)obj->content;
	printf("Cmd: %s\n", cmd->type);
}

// void		exec_f_cmd(void *wrap, void *data)
// {
// 	t_command 		*cmd;
// 	t_stacks		*s;
// 	t_object		*obj;

// 	obj = (t_object *)wrap;
// 	cmd = (t_command *)obj->content;
// 	s = (t_stacks *)data;
// 	cmd->run(s->a, s->b);

// }

void		exec_f_cmd(void *wrap, void *data)
{
	t_command 		*cmd;
	t_stacks		*s;
	t_advstack		*adv_a;
	t_advstack		*adv_b;
	t_object		*obj;

	obj = (t_object *)wrap;
	cmd = (t_command *)obj->content;
	s = (t_stacks *)data;
	adv_a = (t_advstack *)s->a;
	adv_b = (t_advstack *)s->b;
	cmd->run(adv_a->s, adv_b->s);

}

/*
* Defines an input string as a command type and controls it.
*  \param SEARCH_ADD allow to search input string
* in command dictionary and if there is, add it to the executable list.
*  \param PRINT display all execute commands in standart output.
*  \param EXEC execute all commands in queue order.
*/
void		command_controller(t_cmd_action action, void *type, t_stacks *s)
{
	static t_list	*cmd_exec;
	t_llist		*cmd_dict;
	t_list			*sel_lst;
	t_object		*selected_obj;

	if (action == SEARCH_ADD)
	{
		cmd_dict = commands_dictionary(GETALL, NULL, 0);
		// selected_obj = ft_lstselect(cmd_dict, select_by_type, type);
		sel_lst =  ft_select(cmd_dict->enumerable, cmd_dict, select_by_type, type);
		if (!selected_obj)
			exit_with_err(ft_joinargs(3, "There is no command \"",
			(char *)type ,"\" in commands dictionary."));
		if (!cmd_exec)
			cmd_exec = new_list(4096, cmd_dict->content_size);
		cmd_exec->add(cmd_exec, sel_lst->index_of(sel_lst, 0));
	}
	else if (action == EXEC && cmd_exec)
	{
		foreach(cmd_exec->enumerable, cmd_exec, exec_f_cmd, s);
		// cmd_exec = NULL;
		free(cmd_exec);
	}
	else if (action == PRINT && cmd_exec)
		foreach(cmd_exec->enumerable, cmd_exec, exec_f_cmd, s);
	else
		log_add(WARNING, "Can't run or print empty command list.");
}

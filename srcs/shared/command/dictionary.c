#include "command.h"

void		add_command(t_llist **commands, t_command *command)
{
	t_object	*cmd_wrap;

	if (!*commands || !(*commands)->head)
		*commands = new_llist(sizeof(t_command));
	(*commands)->add(*commands, command);
}

// void		remove_command(t_llist **commands, size_t index)
// {

// }

t_command	*get_command(t_llist *commands, size_t index)
{
	t_command	*command;
	t_object 		*ptr;
	
	ptr = *(commands->head);
	while (index)
	{
		ptr = ptr->next;
		index--;
	}
	command = (t_command *)ptr->content;
	return (command);
}

t_llist	*commands_dictionary(t_dic_action action, t_command *command,
							size_t index)
{
	static t_llist	*commands;
	t_llist			*ptr;

	if (action == ADD && command)
		add_command(&commands, command);
	// else if (action == REMOVE)
	// 	remove_command(&commands, index);
	// else if (action == GET)
	// 	return (get_command(&commands, index));
	else if (action == GETALL)
		return (commands);
	return (NULL);
}

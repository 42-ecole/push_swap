/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/09 18:53:25 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/23 11:27:27 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

# include <stdio.h>
# include <stdlib.h>
# include <stdbool.h>
# include "libft.h"
# include "liblog.h"
# include "operator.h"

typedef struct s_advstack
{
	t_stack		*s;
	t_stack 	*chunks;
	void		(*push)(struct s_advstack *self, struct s_advstack *);
	void		(*swap)(struct s_advstack *self, struct s_advstack *);
	void		(*rot)(struct s_advstack *self, struct s_advstack *);
	void		(*rev_rot)(struct s_advstack *self, struct s_advstack *);
	int			(*push_with_if)(struct s_advstack *self, struct s_advstack *,
								int, int);
}				t_advstack;

t_advstack	*new_advstack(void (*push)(t_advstack *, t_advstack *),
void (*swap)(t_advstack *, t_advstack *), void (*rot)(t_advstack *, t_advstack *),
void (*rrot)(t_advstack *, t_advstack *),
int (*funcif)(t_advstack *self, t_advstack *dst, int size, int chunks_count));

int	push_all_more_mid(t_advstack *src, t_advstack *dst,
						 int new_size, int chunks_count);
int	push_all_less_mid(t_advstack *src, t_advstack *dst,
						 int new_size, int chunks_count);

typedef struct s_stacks
{
	t_advstack	*a;
	t_advstack	*b;
}				t_stacks;

typedef enum
{
	ADD,
	REMOVE,
	GET,
	GETALL
}				t_dic_action;

typedef struct	s_command
{
	char		*type;
	void		(*run)(t_stack *a, t_stack *b);
}				t_command;

t_command	*new_command(char *type, void (*run)(t_stack *a, t_stack *b));

t_llist	*commands_dictionary(t_dic_action action, t_command *command,
							size_t index);

typedef enum
{
	SEARCH_ADD,
	EXEC,
	PRINT
}				t_cmd_action;


typedef struct s_hash_data
{
	unsigned int		num;
	int					argc;
}				t_hash_data;

/*
* Defines an input string as a command type and controls it.
*  \param SEARCH_ADD allow to search input string
* in command dictionary and if there is, add it to the executable list.
*  \param PRINT display all execute commands in standart output.
*  \param EXEC execute all commands in queue order.
*/
void	command_controller(t_cmd_action action, void *type, t_stacks *stacks);

void	init_commands();
void	print_commands();
bool	parse_command(char *str);

bool		is_valid_arg(char *arg, int *out, t_hash_table *htable);
void    	is_stk_ascorder(t_stack *s);
void		exit_with_err(char *str);
t_stacks	*input_to_stack(int argc, char *argv[]);

t_advstack	*new_adv_a(int capacity, int content_size);
t_advstack	*new_adv_b(int capacity, int content_size);
bool		gen_sub_chunk(t_advstack *src, t_advstack *dst, int size, int depth);
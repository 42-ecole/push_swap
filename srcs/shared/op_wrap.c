/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op_wrap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 16:03:52 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/22 15:18:38 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"

void		swap_a(t_advstack *a, t_advstack *b)
{
	int		num;
	int		num2;

	num = *CAST_TYPE(int,
		a->s->seq->index_of(a->s->seq, a->s->seq->count - 1));
	num2 = *CAST_TYPE(int,
		a->s->seq->index_of(a->s->seq, a->s->seq->count - 2));
	if (num2 < num)
	{
		sa(a->s, b->s);
		printf("sa\n");
	}
}

void		swap_b(t_advstack *b, t_advstack *a)
{
	int		num;
	int		num2;

	num = *CAST_TYPE(int,
		b->s->seq->index_of(b->s->seq, b->s->seq->count - 1));
	num2 = *CAST_TYPE(int,
		b->s->seq->index_of(b->s->seq, b->s->seq->count - 2));
	if (num2 > num)
	{
		sb(a->s, b->s);
		printf("sb\n");
	}
}

void		push_a(t_advstack *b, t_advstack *a)
{
	pa(a->s, b->s);
	printf("pa\n");
}

void		push_b(t_advstack *a, t_advstack *b)
{
	pb(a->s, b->s);
	printf("pb\n");
}

void		rot_a(t_advstack *a, t_advstack *b)
{
	ra(a->s, b->s);
	printf("ra\n");
}

void		rot_b(t_advstack *b, t_advstack *a)
{
	rb(a->s, b->s);
	printf("rb\n");
}

void		rev_rot_a(t_advstack *a, t_advstack *b)
{
	rra(a->s, b->s);
	printf("rra\n");
}

void		rev_rot_b(t_advstack *b, t_advstack *a)
{
	rrb(a->s, b->s);
	printf("rrb\n");
}

t_advstack	*new_adv_a(int capacity, int content_size)
{
	t_advstack *a;

	a = new_advstack(push_b, swap_a, rot_a, rev_rot_a, push_all_less_mid);
	a->s = new_stack(capacity, content_size);
	a->s->seq->is_sorted = lst_is_descorder;
	return (a);
}

t_advstack	*new_adv_b(int capacity, int content_size)
{
	t_advstack *b;

	b = new_advstack(push_a, swap_b, rot_b, rev_rot_b, push_all_more_mid);
	b->s = new_stack(capacity, content_size);
	b->s->seq->is_sorted = lst_is_ascorder;
	return (b);
}


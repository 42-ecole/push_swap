/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adv_stack.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/13 14:27:56 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/19 12:30:15 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"

int		get_middle(t_list *lst, int start, int len)
{
	int		*mid;
	t_list	*sorted;

	sorted = new_sublist(lst, start, len);
	lst_sort(sorted, 0, sorted->count - 1);
	mid = (int *)sorted->index_of(sorted, sorted->count / 2);
	free(sorted);
	return (*mid);
}

int		push_all_more_mid(t_advstack *src, t_advstack *dst,
						 int size, int chunks_count)
{
	int		mid;
	int		num;
	int		to_reverse;
	int		new_size;
	int		remain_size;
	
	to_reverse = 0;
	new_size = size / 2;
	remain_size = size - new_size;
	if (!(size % 2))
		new_size--;
	mid = get_middle(src->s->seq, src->s->seq->count - size, size);
	while (new_size)
		{
			num = *CAST_TYPE(int, src->s->peek(src->s));
			if (num > mid)
			{
				src->push(src, dst);
				while (to_reverse && chunks_count)
				{
					src->rev_rot(src, dst);
					to_reverse--;
				}
				new_size--;
			}
			else
			{
				src->rot(src, dst);
				to_reverse++;
			}
		}
	return (remain_size);
}

int		push_all_less_mid(t_advstack *src, t_advstack *dst,
						 int size, int chunks_count)
{
	int		mid;
	int		num;
	int		to_reverse;
	int		new_size;
	int		remain_size;

	to_reverse = 0;
	new_size = size / 2;
	remain_size = size - new_size;
	mid = get_middle(src->s->seq, src->s->seq->count - size, size);
	while (new_size)
		{
			num = *CAST_TYPE(int, src->s->peek(src->s));
			if (num < mid)
			{
				src->push(src, dst);
				while (to_reverse)
				{
					src->rev_rot(src, dst);
					to_reverse--;
				}
				new_size--;
			}
			else
			{
				src->rot(src, dst);
				to_reverse++;
			}
		}
	return (remain_size);
}

// rework all method
t_advstack	*new_advstack(void (*push)(t_advstack *, t_advstack *),
void (*swap)(t_advstack *, t_advstack *), void (*rot)(t_advstack *, t_advstack *),
void (*rrot)(t_advstack *, t_advstack *),
int (*funcif)(t_advstack *self, t_advstack *dst, int size, int chunks_count))
{
	t_advstack	*s;

	s = ft_memalloc(sizeof(t_advstack));
	s->chunks = new_stack(100, sizeof(int));
	s->push = push;
	s->swap = swap;
	s->rot = rot;
	s->rev_rot = rrot;
	s->push_with_if = funcif;
	return (s);
}
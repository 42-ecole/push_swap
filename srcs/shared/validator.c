/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   validator.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/24 16:09:04 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/20 12:03:37 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "command.h"
#include <stdio.h>

void		exit_with_err(char *str)
{
	ft_putstr("Error\n");
	log_add(FATAL, str);
}

bool	is_duplicate_int(int num, t_hash_table *htable)
{
	int			i;
	t_llist		*lst;
	t_hash_data	hash_data;
	t_hash_data	founded;

	hash_data.argc = htable->count;
	hash_data.num = num;
	i = -1;
	if ((lst = CAST_TYPE(t_llist, htable->search(htable, &hash_data)))
	&& lst->count > 0)
	{
		lst->enumerable.reset(lst);
		while (++i < lst->count)
		{
			// Fuck this shit i'm out; TODO rework unpackage from object
			t_object *obj = CAST_TYPE(t_object, lst->enumerable.current(lst));
			founded = *CAST_TYPE(t_hash_data, obj->content);
			if (num == founded.num)
				return (true);
			lst->enumerable.move_next(lst);
		}
	}
	htable->add(htable, &hash_data);
	return (false);
}

bool	is_valid_arg(char *arg, int *out, t_hash_table *htable)
{
	int64_t tmp;
	
	if (!ft_isnum(arg) || ft_strlen(arg) > 11)
		exit_with_err("The argument wasn't integer or \
his symbolic length more than integer.");
	tmp = ft_atol(arg);
	if (tmp > INT_MAX || tmp < INT_MIN)
		exit_with_err("The argument was outside the integer range.");
	*out = (int)tmp;
	if (is_duplicate_int(*out, htable))
		exit_with_err("The argument was duplicated.");
	return (true);
}

bool	is_valid_command(char *cmd)
{
	return (true);
}

void    is_stk_ascorder(t_stack *s)
{
    if (s->seq->is_sorted(s->seq))
        printf("OK\n");
    else
        printf("KO\n");
}

unsigned int	hash_int(void *data)
{
	t_hash_data		hash_data;

	hash_data = *CAST_TYPE(t_hash_data, data);
	return (hash_data.num % hash_data.argc);
}

char		**get_argv_without_name(int argc, char *argv[])
{
	char	**new_argv;
	int		i;

	i = 0;
	new_argv = ft_memalloc(sizeof(char *) * argc - 1);
	while (i < argc - 1)
	{
		new_argv[i] = ft_memalloc(sizeof(char) * ft_strlen(argv[i]));
		ft_strcpy(new_argv[i], argv[i + 1]);
		i++;
	}
	return (new_argv);
}

t_stacks	*input_to_stack(int argc, char *argv[])
{
	char			**raw;
	t_stacks		*s;
	t_hash_table 	*htable;
	int				value;
	int				j;

	j = 0;
	if (argc == 2)
		raw = ft_strsplit_withcount(argv[1], ' ', &j);
	else
	{
		raw = get_argv_without_name(argc, argv);
		j = argc - 1;
	}
	htable = new_hash_table(j, sizeof(int), hash_int);
	s = ft_memalloc(sizeof(t_stacks));
	s->a = new_adv_a(j, sizeof(int));
	s->b = new_adv_b(j, sizeof(int));
	while (j)
	{
		if (is_valid_arg(raw[j - 1], &value, htable))
			s->a->s->push(s->a->s, &value);
		j--;
	}
	log_add(INFO, "Arguments are valid and has been initialized in stack.");
	return (s);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/28 16:18:01 by rfunk             #+#    #+#             */
/*   Updated: 2020/11/27 01:13:44 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void	init_commands()
{
	commands_dictionary(ADD, new_command("sa", sa), 0);
	commands_dictionary(ADD, new_command("sb", sb), 0);
	commands_dictionary(ADD, new_command("ss", ss), 0);
	commands_dictionary(ADD, new_command("pa", pa), 0);
	commands_dictionary(ADD, new_command("pb", pb), 0);
	commands_dictionary(ADD, new_command("ra", ra), 0);
	commands_dictionary(ADD, new_command("rb", rb), 0);
	commands_dictionary(ADD, new_command("rr", rr), 0);
	commands_dictionary(ADD, new_command("rra", rra), 0);
	commands_dictionary(ADD, new_command("rrb", rrb), 0);
	commands_dictionary(ADD, new_command("rrr", rrr), 0);
	log_add(INFO, "Commands dictionary has been initialized.");
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/26 17:06:29 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/10 13:41:42 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include <stddef.h>
#include <limits.h>

void		print_fel(void *obj, void *data)
{
	int		num;

	num = *CAST_TYPE(int, obj);
	printf(" %d\n", num);
}

int			main(int argc, char *argv[])
{
	t_stacks	*stacks;
	char		*buf;
	int			val;

	if (argc < 2)
		return (-1);
	init_logger();
	init_commands();
	stacks = input_to_stack(argc, argv);
	buf = ft_memalloc(8192);
	while (((val = scanf("%s", buf)) != EOF) && (val == 1))
		command_controller(SEARCH_ADD, buf, NULL);
	command_controller(EXEC, NULL, stacks);
	is_stk_ascorder(stacks->a);
	log_unload();
	return (0);
}
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logger.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk>                              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/26 22:05:33 by rfunk             #+#    #+#             */
/*   Updated: 2020/12/02 15:11:59 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "liblog.h"

static t_logger g_logger;

void	log_add(enum mes_state state, char *message)
{
	t_log_record	record;

	record.state = state;
	record.date = get_current_time();
	record.message = message;
	g_logger.log->add(g_logger.log, &record);
	if (record.state == FATAL)
	{
		log_unload();
		exit(1);
	}
}

char	*deserialize_log_record(t_log_record record)
{
	char	*str;
	char	*out_state;

	if (record.state == INFO)
		out_state = "INFO";
	else if (record.state == WARNING)
		out_state = "WARNING";
	else if (record.state == ERROR || record.state == FATAL)
		out_state = "ERROR"; 
	str = ft_joinargs(7, "[", record.date, "][",
		out_state, "]: ", record.message, "\n");
	return (str);
}

bool	ft_has_dir(char *path)
{
	DIR	*dir;

	dir = opendir(path);
	if (!dir)
	{
		if (mkdir(path, 0750) < 0)
			return (false);
	}
	return (true);
}

void	log_print_p(void *obj, void *data)
{
	t_log_record	record;
	char			*str;

	record = *CAST_TYPE(t_log_record, obj);
	str = deserialize_log_record(record);
	printf("%s", str);
	free(str);
}

void	log_print()
{
	foreach(g_logger.log->enumerable, g_logger.log,
		log_print_p, NULL);

}

void	log_write_record(void *obj, void *data)
{
	t_log_record	record;
	char			*str;
	FILE			*fp;

	record = *CAST_TYPE(t_log_record, obj);
	fp = CAST_TYPE(FILE, data);
	if(!fp)
	{
        printf("File couldn't be opened\n");
        exit(1);
    }
	str = deserialize_log_record(record);
	fputs(str, fp);
	free(str);
}

void	log_unload()
{
	FILE			*fp;
	char			*root;
	char			*log_dir;
	char			*log_file;

	root = ft_curdir();
	log_dir = ft_joinargs(2, root, "/logs");
	if (ft_has_dir(log_dir))
	{
		log_file = ft_joinargs(2, log_dir, "/latest_log.txt");
		fp = fopen(log_file, "w");
		foreach(g_logger.log->enumerable, g_logger.log,
			log_write_record, fp);
	}
	else
		printf("Error at logger. Can't unload\n");
}



void	init_logger()
{
	g_logger.log = new_list(100,
		sizeof(int)
		+ sizeof(char) * DEFAULT_DATE_LEN
		+ sizeof(char) * DEFAULT_MESSAGE_LEN);
	log_add(INFO, "Logger has been initialized.");
}
CHECKER := checker
PUSH_SWAP := push_swap
GENERATOR := generator
NAME := $(PUSH_SWAP) $(CHECKER)
 
CC := gcc
CC_DEBUG := gcc -g
# FLAGS = -Wall -Wextra -Werror
DEL := rm -rf
SRCS_PATH := ./srcs

#-----------SOURCES---------------#
COM_SRC := command/command_controller.c \
		command/dictionary.c \
		command/command.c \
		operator/sa.c \
		operator/sb.c \
		operator/ss.c \
		operator/pa.c \
		operator/pb.c \
		operator/ra.c \
		operator/rb.c \
		operator/rr.c \
		operator/rra.c \
		operator/rrb.c \
		operator/rrr.c \
		validator.c \
		adv_stack.c \
		op_wrap.c

C_SRC := checker.c \
		init.c

P_SRC := push_swap.c

G_SRC := generator.c

COM_SRC_DIR := $(addprefix $(SRCS_PATH)/shared/, $(COM_SRC))
P_SRC_DIR := $(addprefix $(SRCS_PATH)/push_swap/, $(P_SRC))
C_SRC_DIR := $(addprefix $(SRCS_PATH)/checker/, $(C_SRC))
G_SRC_DIR := $(addprefix $(SRCS_PATH)/generator/, $(G_SRC))

P_OBJ := $(P_SRC_DIR:.c=.o)
C_OBJ := $(C_SRC_DIR:.c=.o)
COM_OBJ := $(COM_SRC_DIR:.c=.o)
G_OBJ := $(G_SRC_DIR:.c=.o)

OBJ := $(COM_OBJ) $(P_OBJ) $(C_OBJ) $(G_OBJ)
#----------------------------------#


#----------LIBRARIES--------------#
LIBFTDIR := libs/libft
LIBFTINC := .
LIBFT := ft
FTMAKE := +make -sC $(LIBFTDIR)/$(LIBFTINC)

LIBLOGDIR := libs/liblog
LIBLOGINC := .
LIBLOG := log
LOGMAKE := +make -sC $(LIBLOGDIR)/$(LIBLOGINC)

LIBS := -L $(LIBLOGDIR) -l $(LIBLOG) -L $(LIBFTDIR) -l $(LIBFT)
INCLUDES := -I $(SRCS_PATH)/checker \
			-I $(SRCS_PATH)/push_swap \
			-I $(SRCS_PATH)/shared \
			-I $(SRCS_PATH)/shared/operator \
			-I $(LIBFTDIR)/$(LIBFTINC) \
			-I $(LIBLOGDIR)/$(LIBLOGINC)
#----------------------------------#


#-----------STYLES-----------------#
NONE = \033[0m
INVERT := \033[7m
GREEN := \033[32m
RED := \033[31m
SUCCESS := [$(GREEN)✓$(NONE)]
SUCCESS2 := [$(INVERT)$(GREEN)✓$(NONE)]
PUSHSWAPOK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(PUSH_SWAP) has been successfully compiled.$(NONE)
CHECKEROK := $(INVERT)$(GREEN)Compiled ✓$(NONE)$(INVERT):$(CHECKER) has been successfully compiled.$(NONE)
APPDELETED := $(INVERT)$(RED)Removed ✓$(NONE)$(INVERT):$(NAME) has been successfully removed.$(NONE)
#----------------------------------#

all:
	@make -s -j4 $(NAME);

$(OBJ): %.o: %.c
	@echo -n $(NAME):' $@: '
	@$(CC) -c $(FLAGS) $(INCLUDES) $< -o $@
	@echo "$(SUCCESS)"

$(LIBFT):
	@$(FTMAKE)

$(LIBLOG):
	@$(LOGMAKE)

$(NAME):  $(OBJ) $(LIBFT) $(LIBLOG)
	@$(CC) $(COM_OBJ) $(P_OBJ) $(LIBS) -o $(PUSH_SWAP)
	@$(CC) $(COM_OBJ) $(C_OBJ) $(LIBS) -o $(CHECKER)
	@$(CC) $(COM_OBJ) $(G_OBJ) $(LIBS) -o $(GENERATOR)
	@echo "$(PUSHSWAPOK)"
	@echo "$(CHECKEROK)"

debug:
	$(CC_DEBUG) $(COM_SRC_DIR) $(C_SRC_DIR) $(LIBS) $(INCLUDES) -g -o $(CHECKER)
	$(CC_DEBUG) $(COM_SRC_DIR) $(P_SRC_DIR) $(LIBS) $(INCLUDES) -g -o $(PUSH_SWAP)
	$(CC_DEBUG) $(COM_SRC_DIR) $(G_SRC_DIR) $(LIBS) $(INCLUDES) -g -o $(GENERATOR)

$(CC_DEBUG):
	@$(eval CC=$(CC_DEBUG))
	debug_all: $(CC_DEBUG) pre
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(NONE)"
	debug: $(CC_DEBUG) all
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(NONE)"

clean:
	@$(DEL) $(OBJ) $(OBJ_DIR)
	@$(FTMAKE) clean

fclean: clean
	@$(FTMAKE) fclean
	@$(DEL) $(NAME)
	@echo "$(APPDELETED)"

norm:
	@norminette $(SRC_DIR)
	@$(FTMAKE) norm

re: fclean all

.PHONY: all fclean clean re pre debug
